from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.webdriver import WebDriver
from po.login_page import LoginPage

from po.base_page import BasePage
from po.register_page import ResigterPage


class NavBar(BasePage):

    _index_link = By.CSS_SELECTOR,'[href="/"]'
    _unread_link = By.CSS_SELECTOR,'[href="/my/messages"]'
    _newman_link = By.CSS_SELECTOR,'[href="/getstart"]'
    _login_link = By.CSS_SELECTOR,'[href="/signin"]'
    _registet_link =By.CSS_SELECTOR, '[href="/signup"]'

    _user_name_link = By.CSS_SELECTOR,'span[class="user_name"]>a.dark'

    def go_to_index_page(self):
        self._driver.find_element(*self._index_link).click()

    def go_unread_page(self):
        self._driver.find_element(*self._unread_link).click()

    def go_to_newmam_page(self):
        self._driver.find_element(*self._newman_link).click()

    def go_to_login_page(self):
        self._driver.find_element(*self._login_link).click()
        return LoginPage(self._driver)

    def go_to_register_page(self):
        self._driver.find_element(*self._registet_link).click()
        return ResigterPage(self._driver)

    def go_to_user_center_page(self):
        self._driver.find_element(*self._user_name_link).click()

    @property
    def user_name_text(self):
        return self._driver.find_element(*self._user_name_link).text

if __name__ == '__main__':
    driver = webdriver.Chrome()
    driver.implicitly_wait(6)
    navbar = NavBar(driver)
    driver.get("http://39.107.96.138:3000/")
    navbar.go_to_newmam_page()
    navbar.go_to_index_page()
