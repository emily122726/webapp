from selenium.webdriver.common.by import By
from po.base_page import BasePage

class LoginPage(BasePage):

    _name_input=By.ID,"name"
    _password_input=By.ID,"pass"
    _login_btn=By.CSS_SELECTOR,'[value="登录"]'

    _failed_tip_text = By.CSS_SELECTOR,'div.alert.alert-error > strong'

    def login_with_username_pass(self,username,passwd):
        self._driver.find_element(*self._name_input).send_keys(username)
        self._driver.find_element(*self._password_input).send_keys(passwd)
        self._driver.find_element(*self._login_btn).click()

    @property
    def login_failed_tip_text(self):
        return self._driver.find_element(*self._failed_tip_text).text
