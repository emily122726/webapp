from selenium.webdriver.common.by import By

from po.base_page import BasePage


class ResigterPage(BasePage):

    _username_input = By.ID,"loginname"
    _pass_input = By.ID,"pass"
    _repass_input = By.ID,"re_pass"
    _email_input = By.ID,"email"

    _submit_btn = By.CSS_SELECTOR,'[value="注册"]'

    _error_tip_text = By.CSS_SELECTOR,'div.alert.alert-error>strong'
    _success_tip_text = By.CSS_SELECTOR,'div.alert.alert-success >strong'

    def register_with_user_info(self,username,passwd,repasswd,email):
        self._driver.find_element(*self._username_input).send_keys(username)
        self._driver.find_element(*self._pass_input).send_keys(passwd)
        self._driver.find_element(*self._repass_input).send_keys(repasswd)
        self._driver.find_element(*self._email_input).send_keys(email)

        self._driver.find_element(*self._submit_btn).click()
        return self


    def get_error_tip_text(self):
        return self._driver.find_element(*self._error_tip_text).text

    @property
    def success_tip_text(self):
        return self._driver.find_element(*self._success_tip_text).text