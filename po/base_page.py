from selenium.webdriver.chrome.webdriver import WebDriver

class BasePage:

    def __init__(self,driver:WebDriver):
        self._driver = driver
        self._driver.set_window_rect(x=0,y=0,width=1280,height=768)