from selenium.webdriver.common.by import By

from po.base_page import BasePage
from po.topic_page import TopicPage


class MainPage(BasePage):

    _create_new_topic_btn = By.ID,"create_topic_btn"


    def go_to_new_topic_page(self):
        self._driver.find_element(*self._create_new_topic_btn).click()
        return TopicPage(self._driver)