from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from po.base_page import BasePage


class TopicPage(BasePage):

    _tab_optiob = By.ID,"tab-value"
    _title_textarea = By.ID,"title"
    _content_div = By.CSS_SELECTOR,'div.CodeMirror-scroll'

    _submit_btn = By.CSS_SELECTOR,'[value="提交"]'


    def create_a_new_topic(self,tab,title,content):

        option = self._driver.find_element(*self._tab_optiob)
        Select(option).select_by_value(tab)

        self._driver.find_element(*self._title_textarea).send_keys(title)
        contet_div = self._driver.find_element(*self._content_div)

        contet_div.click()
        ActionChains(self._driver).move_to_element(contet_div).send_keys(content).perform()

        self._driver.find_element(*self._submit_btn).click()