import unittest
from selenium import webdriver

from po.main_page import MainPage
from po.navbar import NavBar

# 继承 unittest.TestCase
class TestTopic(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        """
        运行所有的用例之前执行
        :return:
        """
        # 1. 打开浏览器
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls) -> None:
        """
        所有的用例执行完成之后
        :return:
        """
        cls.driver.quit()


    def setUp(self) -> None:
        """
        每一个TestCase 执行之前的操作
        :return:
        """
        # 删除所有的cookies
        self.driver.delete_all_cookies()
        self.driver.get("http://39.107.96.138:3000/")

    def tearDown(self) -> None:
        """
        每一个case 运行之后的操作
        :return:
        """
        # TODO 将截图文件放到 screenshots 目录下，要求如下：
        # 1. 以当前日期为图片的文件 日期格式 2020-05-17_14_32_29.png
        # 2. 图片格式为png格式
        self.driver.save_screenshot('01.png')


    def test_topic(self):
        navBar = NavBar(self.driver)

        loginPage = navBar.go_to_login_page()
        # 4. 使用正确的用户名和密码登录
        loginPage.login_with_username_pass(username="imtest11", passwd="123456")
        mainpage = MainPage(self.driver)
        topicpage= mainpage.go_to_new_topic_page()
        topicpage.create_a_new_topic('share',"helloworld1234567","11111111111111")

        #TODO 添加断言