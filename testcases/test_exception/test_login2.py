"""
测试登录的异常场景
用户名正确+密码错误
用户名错误+密码错误
用户名错误+密码正确
"""
test_data=[
    ('imtest','12'),
    ('im','12'),
    ('im','123456'),
    ('',''),
    ('imtest11',''),
    ('','123456'),
    ('#$@!@#!@','2@#@!23')
]

"""
根据test_data 中的数据 动态生成测试用例（testcase）
当test_data 中有5条数据的时候，能够动态产生5条case
"""

import  unittest
from po.navbar import NavBar
from testcases.test_exception.base_testcase import BaseTestCase

# 导入相关的库
from ddt import ddt,data

@ddt
class TestLogin2(BaseTestCase):

    def login(self,username,passwd,exceptval):
        navbar = NavBar(self.driver)
        loginpage = navbar.go_to_login_page()
        loginpage.login_with_username_pass(username=username, passwd=passwd)
        msg = loginpage.login_failed_tip_text
        self.assertEqual(msg, exceptval)

    @data(
        ('imtest','12',"用户名或密码错误"),
        ('im','12',"用户名或密码错误"),
        ('im','123456',"用户名或密码错误"),
        ('','',"信息不完整。"),
        ('imtest11','',"信息不完整。"),
        ('','123456',"信息不完整。"),
        ('#$@!@#!@','2@#@!23',"用户名或密码错误"),
        ('&&&***','123456',"用户名或密码错误")
    )
    def test_login(self,value):
        self.login(username=value[0],passwd=value[1],exceptval=value[2])


