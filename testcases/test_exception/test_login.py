"""
用户名正确+密码错误
用户名错误+密码错误
用户名错误+密码正确

使用封装函数的方式将操作内容单独抽离出来,
在每一个case上面 调用公共方法，通过传入不同的参数值进行功能校验。
"""

import unittest
from testcases.test_exception.base_testcase import BaseTestCase
from po.navbar import NavBar

class TestLoginException(BaseTestCase):


    def login(self,username,passwd,exceptval):
        navbar = NavBar(self.driver)
        loginpage = navbar.go_to_login_page()
        loginpage.login_with_username_pass(username=username, passwd=passwd)
        msg = loginpage.login_failed_tip_text
        self.assertEqual(msg, exceptval)

    def test_login_correct_username_error_passwd(self):
        """
        用户名正确+密码错误
        """
        self.login(username='imtest11',passwd='12',exceptval='用户名或密码错误')

    def test_login_error_username_error_passwd(self):
        """
        用户名错误+密码错误
        :return:
        """
        self.login(username='imt1', passwd='12', exceptval='用户名或密码错误')

    def test_login_error_username_correct_passwd(self):
        """
        用户名错误+密码正确
        :return:
        """
        self.login(username='imte', passwd='123456', exceptval='用户名或密码错误')