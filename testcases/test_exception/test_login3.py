import unittest

from po.navbar import NavBar
from testcases.test_exception.base_testcase import BaseTestCase
from ddt import  ddt,file_data
from common.file_utils import get_data_file_path

@ddt
class TestLogin(BaseTestCase):

    def login(self,username,passwd,exceptval):
        navbar = NavBar(self.driver)
        loginpage = navbar.go_to_login_page()
        loginpage.login_with_username_pass(username=username, passwd=passwd)
        msg = loginpage.login_failed_tip_text
        self.assertEqual(msg, exceptval)
    @file_data(get_data_file_path('login_data.json'))
    def test_login(self,username,passwd,exceptval):
        self.login(username=username,passwd=passwd,exceptval=exceptval)