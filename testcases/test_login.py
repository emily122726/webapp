import unittest
from selenium import webdriver
from po.navbar import NavBar
from ddt import ddt,data,unpack

# 继承 unittest.TestCase
@ddt
class TestLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        """
        运行所有的用例之前执行
        :return:
        """
        # 1. 打开浏览器
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls) -> None:
        """
        所有的用例执行完成之后
        :return:
        """
        cls.driver.quit()


    def setUp(self) -> None:
        """
        每一个TestCase 执行之前的操作
        :return:
        """
        # 删除所有的cookies
        self.driver.delete_all_cookies()
        self.driver.get("http://39.107.96.138:3000/")

    def tearDown(self) -> None:
        """
        每一个case 运行之后的操作
        :return:
        """
        # TODO 将截图文件放到 screenshots 目录下，要求如下：
        # 1. 以当前日期为图片的文件 日期格式 2020-05-17_14_32_29.png
        # 2. 图片格式为png格式
        self.driver.save_screenshot('01.png')



    def test_login(self):
        """
        测试用户登录
        :return:
        """
        # 3. 导航到登录页面
        navBar = NavBar(self.driver)
        loginPage = navBar.go_to_login_page()
        # 4. 使用正确的用户名和密码登录
        loginPage.login_with_username_pass(username="imtest11",passwd="123456")
        # 5.1 登录成功 期望页面能够跳转到首页
        url = self.driver.current_url
        self.assertEqual(url,'http://39.107.96.138:3000/')
        # 5.2 登录成功 期望首页页面上个人信息栏中 imtest11 用户名
        username = navBar.user_name_text
        print("username",username)
        self.assertEqual(username,'imtest11')

    @data(('imxxxx','123456'),('imtest11','1112222'),('',''))
    @unpack
    def test_login_failed(self,name,passwd):
        navBar = NavBar(self.driver)
        loginPage = navBar.go_to_login_page()
        # 4. 使用正确的用户名和密码登录
        loginPage.login_with_username_pass(username=name, passwd=passwd)
        # 5.1 登录失败 验证失败信息
        text = loginPage.login_failed_tip_text
        self.assertEqual(text,'用户名或密码错误')




if __name__ == '__main__':
    unittest.main()