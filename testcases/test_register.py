import unittest
from po.navbar import NavBar
from selenium import webdriver
from common.file_utils import get_screenshot_png

class TestResgiter(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """
        运行所有的用例之前执行
        :return:
        """
        # 1. 打开浏览器
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls) -> None:
        """
        所有的用例执行完成之后
        :return:
        """
        cls.driver.quit()


    def setUp(self) -> None:
        """
        每一个TestCase 执行之前的操作
        :return:
        """
        # 删除所有的cookies
        self.driver.delete_all_cookies()
        self.driver.get("http://39.107.96.138:3000/")

    def tearDown(self) -> None:
        """
        每一个case 运行之后的操作
        :return:
        """
        # TODO 将截图文件放到 screenshots 目录下，要求如下：
        # 1. 以当前日期为图片的文件 日期格式 2020-05-17_14_32_29.png
        # 2. 图片格式为png格式


        self.driver.save_screenshot(get_screenshot_png())


    def test_register(self):

        # 3. 导航到注册页面
        navbar = NavBar(self.driver)
        resigter_page = navbar.go_to_register_page()
        # 4. 输入正确的信息并提交
        resigter_page.register_with_user_info(username="xiaoming123",
                                              passwd="123456",
                                              repasswd="123456",
                                              email="xiaoming123@163.com")
        # 5. 注册成功显示信息
        text = resigter_page.success_tip_text
        self.assertEqual(text,"欢迎加入 Nodeclub！我们已给您的注册邮箱发送了一封邮件，请点击里面的链接来激活您的帐号。")

