"""
测试用例集
组织测试用例

"""
import unittest
from testcases.test_login import TestLogin
from testcases.test_register import TestResgiter


def suite():
    testsuite = unittest.TestSuite()  # 创建测试套件
    # addTest 方法添加单个测试用例 格式：  类名('方法名')
    # 使用addTest的方式只适合小型测试场景。同时用很多个测试用例 就不太适合了。
    testsuite.addTest(TestLogin('test_login'))  # TestLogin中 test_login的测试方法
    testsuite.addTest(TestResgiter('test_register')) # TestLogin中 test_register的测试方法
    return testsuite

def suite2():
    """
    当测试用例文件特别多的时候， 使用 TestLoader 搜寻Testcase
    :return:
    """
    suite =unittest.TestSuite()
    loader = unittest.TestLoader()
    # 从类中加载所有的测试用例 loadTestsFromTestCase 格式： 传入 类名
    testlogins = loader.loadTestsFromTestCase(TestLogin)
    testregisters = loader.loadTestsFromTestCase(TestResgiter)
    suite.addTests(testlogins)
    suite.addTests(testregisters)

    return suite

def login_failed_suite():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    tests = loader.loadTestsFromTestCase(TestLogin)
    suite.addTests(tests)
    return suite

if __name__ == '__main__':
    with open('report.txt',mode='w',encoding='utf8') as f:
        runner = unittest.TextTestRunner(stream=f,verbosity=2)  # 用例执行器
        suite = login_failed_suite()
        runner.run(suite) # 运行测试套件