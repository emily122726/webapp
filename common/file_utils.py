"""
定义文件操作相关的工具类

"""


import time
import os
def get_screenshot_png():
    root = os.path.dirname(os.path.dirname(__file__))
    filename = time.strftime('%Y-%m-%d_%H_%M_%S', time.localtime(time.time()))
    return os.path.join(root,'screenshots',filename+'.png')

def get_data_file_path(filename):
    """
    传数据文件名称，返回测试数据文件的绝对路径。
    :param filename: 数据驱动文件的名称
    :return:
    """
    root_dir = os.path.dirname( os.path.dirname(__file__))
    file_path = os.path.join(root_dir,'data',filename)
    if os.path.exists(file_path):
        return file_path
    else:
        raise Exception(f'文件{filename}不存在')


if __name__ == '__main__':
    print(get_data_file_path('login_data1.json'))